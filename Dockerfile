FROM node:10.15.1
COPY dist/ ./dist/
COPY package*.json ./
COPY production.js ./

EXPOSE 8088

CMD [ "npm", "run", "server" ]
