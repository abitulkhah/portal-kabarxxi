import { AppBrowserModule } from './app/app.browser.module';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NgxAnalyticsGoogleAnalytics } from 'ngx-analytics/ga';

import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
  NgxAnalyticsGoogleAnalytics.prototype.createGaSession(environment.googleAnalytics);
}

document.addEventListener('DOMContentLoaded', () => {
  platformBrowserDynamic().bootstrapModule(AppBrowserModule)
    .catch(err => console.error(err));
});

  // window.console.log = function () { };
