export const environment = {
  production: true,
  googleAnalytics: {
    domain: 'auto',
    trackingId: 'UA-157184307-1' // replace with your Tracking Id
  }
};
